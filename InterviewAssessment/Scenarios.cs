﻿using System.Collections.Generic;
using System.Linq;

namespace InterviewAssessment
{
    public class Scenarios
    {
        /// <summary>
        /// Implement this method.
        /// Uses linq to select each character in a string, converts each character to a string and passes it to aggregator 
        /// which applies a lamba expression that concatenates the aggregated string on to the end of each character
        /// </summary>
        public string Scenario1(string str) => str.Select(ch => ch.ToString()).Aggregate<string>((x, y) => y + x);

        /// <summary>
        /// Fix this method.
        /// </summary>
        public int Scenario2(int @base, int exponent)
        {
            int n = 1;

            for (int i = 0; i < exponent; i++)
            {
                n *= @base;
            }

            return n;
        }

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3<T>(T obj) => typeof(T).Name;

        /// <summary>
        /// DO NOT MODIFY
        /// </summary>
        public string Scenario3(string str) => str;

        /// <summary>
        /// Implement this method.
        /// </summary>
        public string Scenario4(Node node)
        {
            //I think there is a way to do this with a lambda expression
            //but I couldn't get it to work
            if(node.GetType().Name == "Tree"){
                var tree = (Tree)node;
                return Traverse(tree.Text, tree.Children);
            }
            else
            {
                return node.Text;
            }

        }
        
        private string Traverse(string s, IEnumerable<Node> c)
        {
            var result = s;
            foreach(var child in c)
            {
                if(child.GetType().Name == "Tree")
                {
                    var tree = (Tree)child;
                    result = result + '-' + Traverse(tree.Text, tree.Children);
                }
                else
                {
                    result = result + '-' + child.Text;
                }
            }
            return result;
        }
    }

    public class Node {  
        public Node(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class Tree : Node
    {
        public Tree(string text, params Node[] children) : base(text)
        {
            Children = children;
        }

        public IEnumerable<Node> Children { get; set; }
    }
}
